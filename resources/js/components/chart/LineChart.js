import {Line, mixins} from 'vue-chartjs'

const {reactiveProp} = mixins;

export default {
  extends: Line,

  mixins: [reactiveProp],

  props: {
    // Можно дополнить или полностью переопределить localOptions. Доступные опции: https://www.chartjs.org/docs/latest/general/responsive.html#configuration-options
    options: {
      type: Object,
      default: () => ({})
    }
  },

  data: () => ({
    localOptions: {
      responsive: true,
      maintainAspectRatio: false,
      legend: {
        position: "right"
      }
    }
  }),

  computed: {
    combinedOptions() {
      const {localOptions, options} = this;
      return {...localOptions, ...options};
    }
  },

  methods: {
    renderLineChart() {
      this.renderChart(this.chartData, this.combinedOptions)
    }
  },

  mounted() {
    this.renderLineChart();
  },

  watch: {
    chartData() {
      this.renderLineChart();
    }
  }


}
