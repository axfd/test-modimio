import axios from "axios";

const api = axios.create({
    baseURL: '/api',
});

const getMainData = (request = {}) => {
    return api.post('/get-main-data', request).then(({data}) => data)
};

const getChartData = (request = {}) => {
    return api.post('/get-chart-data', request).then(({data}) => data)
};

const getFiltersData = () => {
    return api.get('/get-filters-data').then(({data}) => data)
};

export {
    getMainData,
    getChartData,
    getFiltersData
}
