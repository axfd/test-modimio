<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $guarded = ['id'];

    public function scopeFilter($query, $filters = [])
    {
        if (!$filters) return $query;

        if (isset($filters['os'])) {
            $filter = $filters['os'] === "Не указана" ? null : $filters['os'];
            $query->where('os', $filter);
        }

        if (isset($filters['arch'])) {
            $filter = $filters['arch'] === "Не указана" ? null : $filters['arch'];
            $query->where('arch', $filter);
        }

        if (isset($filters['startDate'])) {
            $date = (Carbon::parse($filters['startDate']))->setTimezone(new \DateTimeZone('Europe/Moscow'));
            $query->where('time', '>', $date);
        }

        if (isset($filters['endDate'])) {
            $date = (Carbon::parse($filters['endDate']))->setTimezone(new \DateTimeZone('Europe/Moscow'))->addDays();
            $query->where('time', '<', $date);
        }

    }
}
