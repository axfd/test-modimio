<?php

namespace App\Jobs;

use App\Models\Log;
use App\Services\Parser\LogRowParser;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class LogParse implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $stack = [];

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $arStack = [])
    {
        $this->stack = array_filter($arStack, function ($item) {
            return $item instanceof LogRowParser;
        });
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = array_filter(array_map(function ($row) {
            return $row->parse();
        }, $this->stack), function ($item) {
            return $item !== false;
        });

        Log::insert($data);
    }
}
