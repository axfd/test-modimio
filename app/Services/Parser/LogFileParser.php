<?php


namespace App\Services\Parser;


use App\Jobs\LogParse;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class LogFileParser
{
    const STACK_SIZE = 50;

    public static function parse($file)
    {
        if (file_exists($file)) {
            if ($handle = @fopen($file, "r")) {
                $arStack = [];

                while (!feof($handle) && (($row = fgets($handle)) !== false)) {
                    $arStack[] = new LogRowParser($row);

                    if (count($arStack) === self::STACK_SIZE) {
                        self::toJob($arStack);
                        $arStack = [];
                    }
                }

                if (count($arStack)) {
                    self::toJob($arStack);
                }
                fclose($handle);

                self::toComplete($file);
            }
        }
    }

    protected static function toJob($arStack)
    {
        dispatch(new LogParse($arStack));
    }

    protected static function toComplete($file)
    {
        $path = File::dirname($file);
        $filename = File::name($file) . "." . File::extension($file);
        $pathComplete = "$path/complete";

        if (!File::exists($pathComplete)) {
            File::makeDirectory($pathComplete);
        }

        File::move($file, $pathComplete . "/" . $filename);
    }
}
