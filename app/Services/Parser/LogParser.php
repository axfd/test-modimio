<?php


namespace App\Services\Parser;


use App\Jobs\LogFileParse;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class LogParser
{
    protected $path;

    public function __construct($path = '')
    {
        $this->path = $path;
    }

    public function execute()
    {
        try {
            if (Storage::exists($this->path)) {
                $files = Storage::files($this->path);
                foreach ($files as $file) {
                    dispatch(new LogFileParse(Storage::path($file)));
                }
            }
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
        }

    }


}
