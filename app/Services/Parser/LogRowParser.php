<?php


namespace App\Services\Parser;

use Illuminate\Support\Carbon;
use Jenssegers\Agent\Agent;


class LogRowParser
{
    protected $content;

    public function __construct($row = '')
    {
        $this->content = $row;
    }

    protected function getArch($content = '')
    {
        return preg_match('/^.*(x64|x86).*$/', $content, $matches) ? $matches[1] : null;
    }

    public function parse()
    {
        $matches = [];
        $regx = '/(?<ip>[0-9.]+)\s+\-\s+\-\s\[(?<time>[^\]]+)\]\s"(?<request>[^"]+)"\s(?<code>\d+)\s+(?<size>\d+)\s"(?<url>[^"]+)"\s"(?<useragent>[^"]+)"/m';
        preg_match_all($regx, $this->content, $matches, PREG_SET_ORDER, 0);

        if (!isset($matches[0]) || !isset($matches[0]['ip'])) return false;

        $agent = new Agent();
        $agent->setUserAgent($matches[0]['useragent']);

        $browser = $agent->browser();
        $os = $agent->platform();

        return [
            'ip' => $matches[0]['ip'],
            'time' => Carbon::parse($matches[0]['time']),
            'url' => preg_replace('/\?.*$/', '', $matches[0]['url']),
            'os' => $os ? $os : null,
            'arch' => $this->getArch($matches[0]['useragent']),
            'browser' => $browser ? $browser : null
        ];
    }

}
