<?php

namespace App\Http\Controllers;

use App\Models\Log;
use Illuminate\Http\Request;

class FiltersController extends Controller
{
    public function getData()
    {
        $os = Log::select('os')->groupBy('os')->get()->pluck('os')->map(function ($item){
            return $item ?? 'Не указана';
        });
        $arch = Log::select('arch')->groupBy('arch')->get()->pluck('arch')->map(function ($item){
            return $item ?? 'Не указана';
        });

        return response()->json(['os' => $os, 'arch' => $arch]);
    }
}
