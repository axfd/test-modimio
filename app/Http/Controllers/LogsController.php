<?php

namespace App\Http\Controllers;

use App\Http\Requests\MainDataRequest;
use App\Models\Log;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LogsController extends Controller
{
    public function getMainData(MainDataRequest $request)
    {
        $query = Log::select(DB::raw('url, browser, DATE_FORMAT(time,"%Y-%c-%e") as date'))
            ->filter($request->filters);

        $queryBrowsersCount = DB::table($query)
            ->select(DB::raw('count(*) as count, date,  browser'))
            ->orderByDesc('count')
            ->groupBy(['date', 'browser']);

        $queryUrlsCount = DB::table($query)
            ->select(DB::raw('count(*) as count, date, url'))
            ->orderByDesc('count')
            ->groupBy(['date', 'url']);

        $cnt = DB::table($query)
            ->select(DB::raw('count(*) as cnt, date'))
            ->groupBy('date');


        $result = DB::table($cnt, 'cnt')
            ->select(['cnt', 'cnt.date']) //, 'browsers.browser as browser', 'urls.url as url'
            ->addSelect([
                'browser' => DB::table($queryBrowsersCount)
                    ->select(DB::raw('browser'))
                    ->orderByDesc('count')
                    ->whereRaw('date = cnt.date')
                    ->limit(1),
                'url' => DB::table($queryUrlsCount)
                    ->select(DB::raw('url'))
                    ->orderByDesc('count')
                    ->whereRaw('date = cnt.date')
                    ->limit(1)
            ]);

        $sorting = $request->sorting;
        if (isset($sorting['sort']) && isset($sorting['order'])) {
            $result->orderBy($sorting['sort'], $sorting['order'] === 'desc' ? 'desc' : 'asc');
        }

        return response()->json(['logs' => $result->get()->toArray()]);
    }

    public function getChartData(MainDataRequest $request)
    {
        $query = Log::select(DB::raw('browser, DATE_FORMAT(time,"%Y-%c-%e") as date'))
            ->filter($request->filters);


        $queryPopularBrowsers = DB::table($query)
            ->select(DB::raw('count(*) as count, browser'))
            ->orderByDesc('count')
            ->groupBy('browser')
            ->limit(3);

        $countPopular = DB::table($query, 'base')
            ->select(DB::raw('count(*) as count, base.browser as browser, base.date as date'))
            ->joinSub($queryPopularBrowsers, 'popular', function ($join) {
                $join->on('base.browser', '=', 'popular.browser');
            })
            ->groupBy(['date', 'browser'])
            ->get()
            ->groupBy(['date']);

        $totalCount = DB::table($query)
            ->select(DB::raw('count(*) as cnt,  date'))
            ->groupBy('date')
            ->get();


        $items = $totalCount->map(function ($date) use ($countPopular) {
            $browsers = $countPopular[$date->date]->mapWithKeys(function ($item) use ($date) {
                return [$item->browser => $date->cnt > 0 ? round($item->count / $date->cnt * 100) : 0];
            })->toArray();

            $browsers['main'] = $date->cnt;
            $browsers['date'] = $date->date;
            return $browsers;
        });

        return response()->json(['items' => $items->reduce(function ($result, $item) {
            foreach ($item as $key => $value) {
                if (!isset($result[$key])) $result[$key] = [];
                $result[$key][] = $value;
            }
            return $result;
        }, [])]);
    }
}
