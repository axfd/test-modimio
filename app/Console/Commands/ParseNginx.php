<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ParseNginx extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Запуск парсинга файлов с логами nginx';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $log = new \App\Services\Parser\LogParser(config('parser.path'));
        $log->execute();
        return 0;
    }
}
