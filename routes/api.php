<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('get-main-data', [\App\Http\Controllers\LogsController::class, 'getMainData']);
Route::post('get-chart-data', [\App\Http\Controllers\LogsController::class, 'getChartData']);
Route::get('get-filters-data', [\App\Http\Controllers\FiltersController::class, 'getData']);
